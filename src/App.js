import "./App.css";
import Button from "./components/Button";
import Input from "./components/Input";
import { Component } from "react";
import ClearButton from "./components/ClearButton";
import * as math from "mathjs";
import Header from "./components/Header";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      input: "",
    };
  }
  handleClear() {}
  addToInput = (val) => {
    this.setState({ input: this.state.input + val });
  };
  
  handleEqual = () => {
    let result =this.state.input;
    try {
      result = math.evaluate(this.state.input) 
      
    } catch (error) {
      result = error.message
    }
    this.setState({ input: 
      result
    });
  };
  render() {
    return (
      <div className="App">
        <div className="calc-wrapper">
        <Header>Calcu</Header>
          <Input input={this.state.input}></Input>
          <div className="row">
            <Button handleClick={this.addToInput}>%</Button>
            <Button handleClick={this.addToInput}>(</Button>
            <Button handleClick={this.addToInput}>)</Button>
            <ClearButton handleClear={() => this.setState({ input: "" })}>
              C
            </ClearButton>
          </div>
          <div className="row">
            <Button handleClick={this.addToInput}>7</Button>
            <Button handleClick={this.addToInput}>8</Button>
            <Button handleClick={this.addToInput}>9</Button>
            <Button handleClick={this.addToInput}>/</Button>
          </div>
          <div className="row">
            <Button handleClick={this.addToInput}>4</Button>
            <Button handleClick={this.addToInput}>5</Button>
            <Button handleClick={this.addToInput}>6</Button>
            <Button handleClick={this.addToInput}>*</Button>
          </div>
          <div className="row">
            <Button handleClick={this.addToInput}>1</Button>
            <Button handleClick={this.addToInput}>2</Button>
            <Button handleClick={this.addToInput}>3</Button>
            <Button handleClick={this.addToInput}>-</Button>
          </div>
          <div className="row">
            <Button handleClick={this.addToInput}>.</Button>
            <Button handleClick={this.addToInput}>0</Button>
            <Button className="equal" handleClick={() => this.handleEqual()}>=</Button>
            <Button handleClick={this.addToInput}>+</Button>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
