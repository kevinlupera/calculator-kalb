import React from "react";
import "./Button.css";

const isOperator = (val) => {
  return !isNaN(val) || val === "." || val === "=";
};
const isEqual = (val) => {
  return val === "=";
};
const Button = (props) => (
  <div
    className={`button-wrapper ${
      isOperator(props.children) ? "" : "operator"
    } ${
      isEqual(props.children) ? "equal" : ""
    }`}
    onClick={() => props.handleClick(props.children)}
  >
    {props.children}
  </div>
);
export default Button;
